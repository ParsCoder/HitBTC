HitBTC Library
-------------------

A library for better communication with HitBTC Exchange API
(`HitBTC API Documentation <https://api.hitbtc.com>`_)

Installation:
~~~~~~~~~~~~~~~
    pip install hit-btc


Example:
~~~~~~~~
.. code:: python

    soon