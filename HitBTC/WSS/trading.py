import uuid
from HitBTC.utils import APIData


class Trading(object):

    def __init__(self, core):
        self.core = core
        self.report = None

    def __reports__(self, response: dict = None, snapshot: dict = None, loop: bool = False,
                    **params) -> dict or list:
        symbol = params.get('symbol', None)
        cid = params.get('cid', None)
        if loop:
            method = response.get('method', None)
            error = response.get('error', None)
            if response and method:
                if not snapshot and cid:
                    snapshot = {}
                elif not snapshot and not cid:
                    snapshot = {
                        'open': [],
                        'close': []
                    }

                if method == 'activeOrders':
                    data = response.get('params')
                    _open = snapshot.get('open')

                    for report in data:
                        report['id'] = int(report['id'])
                        report['price'] = float(report['price'])
                        report['quantity'] = float(report['quantity'])
                        report['cumQuantity'] = float(report['cumQuantity'])

                        if symbol:
                            if symbol == report.get('symbol'):
                                _open.insert(len(_open), report)
                        elif cid:
                            if cid == report.get('clientOrderId'):
                                snapshot = report
                        else:
                            _open.insert(len(_open), report)
                elif method == 'report':
                    report = response.get('params')

                    report['id'] = int(report['id'])
                    report['price'] = float(report['price'])
                    report['quantity'] = float(report['quantity'])
                    report['cumQuantity'] = float(report['cumQuantity'])

                    _open = snapshot.get('open', [])
                    _close = snapshot.get('close', [])

                    status = report.get('status')
                    is_open = status == 'new' or status == 'partiallyFilled'

                    def __report__():
                        if is_open:
                            _open.insert(len(_open), report)
                        else:
                            _close.insert(len(_close), report)
                            for _report in _open:
                                _id = report.get('id')
                                if _report.get('id') == report.get('id'):
                                    _open.remove(_report)

                    if symbol:
                        if symbol == report.get('symbol'):
                            __report__()
                    elif cid:
                        if cid == report.get('clientOrderId'):
                            snapshot = report
                    else:
                        __report__()
            elif error:
                error.update({'error': True})
                return error
            return snapshot
        else:
            self.core.subscribe_reports(status=True)
            while True:
                response = self.core.connection.__receive__()
                method = response.get('method', None)
                error = response.get('error', None)

                if response and method:
                    if not snapshot and cid:
                        snapshot = {}
                    elif not snapshot and not cid:
                        snapshot = []

                    if method == 'activeOrders':
                        data = response.get('params', None)
                        if data:
                            self.core.subscribe_reports(status=False)
                            for report in data:
                                report['id'] = int(report['id'])
                                report['price'] = float(report['price'])
                                report['quantity'] = float(report['quantity'])
                                report['cumQuantity'] = float(report['cumQuantity'])

                                if symbol:
                                    if symbol == report.get('symbol'):
                                        snapshot.insert(len(snapshot), report)
                                elif cid:
                                    if cid == report.get('clientOrderId'):
                                        snapshot = report
                                else:
                                    snapshot.insert(len(snapshot), report)

                            return snapshot
                elif error:
                    error.update({'error': True})
                    return error

    def place(self, **params):
        params.update({'CID': uuid.uuid4().hex})
        rid = self.core.__place__(**params)
        while True:
            response = self.core.connection.__receive__()
            error = response.get('error', None)
            if response.get('id') == rid:
                if not error:
                    self.report = response.get('result')
                    self.report = self.__clean__()
                    return self.report
            if error:
                error.update({'error': True})
                code = error.get('code')
                if code == 20008:
                    params.update({'CID': uuid.uuid4().hex})
                    rid = self.core.__place__(**params)
                else:
                    return error
            if response.get('id') == rid:
                if not error:
                    self.report = response.get('result')
                    self.report = self.__clean__()
                    return self.report

    def cancel(self, **params):
        rid = self.core.__cancel__(**params)
        while True:
            response = self.core.connection.__receive__()
            error = response.get('error', None)
            if response.get('id') == rid:
                if not error:
                    self.report = response.get('result')
                    self.report = self.__clean__()
                    return self.report
                error.update({'error': True})
                return error

    def replace(self, **params):
        rid = self.core.__replace__(**params)
        while True:
            response = self.core.connection.__receive__()
            error = response.get('error', None)
            if response.get('id') == rid:
                if not error:
                    self.report = response.get('result')
                    self.report = self.__clean__()
                    return self.report
            if error:
                error.update({'error': True})
                code = error.get('code')
                if code == 20008:
                    params.update({'CID': uuid.uuid4().hex})
                    rid = self.core.__place__(**params)
                else:
                    return error
            if response.get('id') == rid:
                if not error:
                    self.report = response.get('result')
                    self.report = self.__clean__()
                    return self.report

    def get_active_orders(self, **params):
        rid = self.core.get_orders(**params)
        while True:
            response = self.core.connection.__receive__()
            error = response.get('error', None)
            if response.get('id') == rid:
                if not error:
                    result = response.get('result')
                    data = list()
                    for report in result:
                        report['id'] = int(report['id'])
                        report['price'] = float(report['price'])
                        report['quantity'] = float(report['quantity'])
                        report['cumQuantity'] = float(report['cumQuantity'])
                        data.insert(len(data), report)
                    return data
                error.update({'error': True})
                return error

    def get_trading_balance(self, **params):
        rid = self.core.get_trading_balance(**params)
        while True:
            response = self.core.connection.__receive__()
            if response.get('id') == rid:
                result = response.get('result')
                currency = params.get('currency')
                if currency:
                    for _currency in result:
                        if _currency.get('currency') == currency:
                            _currency['reserved'] = float(_currency['reserved'])
                            _currency['available'] = float(_currency['available'])
                            return _currency
                else:
                    data = list()
                    for _currency in result:
                        _currency['reserved'] = float(_currency['reserved'])
                        _currency['available'] = float(_currency['available'])
                        data.insert(len(data), _currency)
                    return data
