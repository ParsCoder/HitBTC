from HitBTC.utils import APIData


class Market(object):

    def __init__(self, core):
        self.core = core

    def get_currencies(self, **params) -> dict or list:
        rid = self.core.get_currencies(**params)
        while True:
            response = self.core.connection.__receive__()
            if response.get('id') == rid:
                return APIData(response).__currencies__()

    def get_symbols(self, **params) -> dict or list:
        rid = self.core.get_symbols(**params)
        while True:
            response = self.core.connection.__receive__()
            if response.get('id') == rid:
                return APIData(response).__symbols__()

    def get_trades(self, **params) -> dict or list:
        rid = self.core.get_trades(**params)
        while True:
            response = self.core.connection.__receive__()
            if response.get('id') == rid:
                return APIData(response).__trades__()

    def __trades__(self, snapshot: dict = None, symbol: str = None, limit: int = 100) -> dict:
        response = self.core.connection.__receive__()
        method = response.get('method', None)
        error = response.get('error', None)
        if method == 'snapshotTrades':
            while True:
                result = APIData(response).__trades__(symbol=symbol, limit=limit)
                if result is not None:
                    return result
                response = self.core.connection.__receive__()
        if method == 'updateTrades':
            while True:
                result = APIData(response).__trades__(symbol=symbol, snapshot=snapshot, limit=limit)
                if result is not None:
                    return result
                response = self.core.connection.__receive__()
        elif error is True:
            return APIData(response).__trades__()
        return dict()

    def __tickers__(self, snapshot: dict = None, symbol: str = None, limit: int = 100) -> dict:
        response = self.core.connection.__receive__()
        method = response.get('method', None)
        error = response.get('error', None)
        if method == 'ticker':
            return APIData(response).__tickers__(symbol=symbol, snapshot=snapshot, limit=limit)
        elif error is True:
            return APIData(response).__tickers__()
        return dict()

    def __orderbook__(self, snapshot: dict = None, symbol: str = None, limit: int = 100) -> dict:
        response = self.core.connection.__receive__()
        method = response.get('method', None)
        error = response.get('error', None)
        if method == 'snapshotOrderbook':
            return APIData(response.get('params')).__orderbook__(symbol=symbol, limit=limit)
        if method == 'updateOrderbook':
            return APIData(response.get('params')).__orderbook__(symbol=symbol, snapshot=snapshot, limit=limit)
        elif error is True:
            return APIData(response).__orderbook__()
        return dict()

    def __candles__(self, snapshot: dict = None, symbol: str = None, limit: int = 100) -> dict:
        response = self.core.connection.__receive__()
        method = response.get('method', None)
        error = response.get('error', None)
        if method == 'snapshotCandles':
            return APIData(response.get('params')).__candles__(symbol=symbol, limit=limit)
        if method == 'updateCandles':
            return APIData(response.get('params')).__candles__(snapshot=snapshot, symbol=symbol, limit=limit)
        if error is True:
            return APIData(response).__candles__()
        return APIData(response).__candles__(snapshot=snapshot, symbol=symbol, limit=limit)
