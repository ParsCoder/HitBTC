import websocket
import json
import time


class Connection(object):

    def __init__(self, url: str = None):

        self.url = url or 'wss://api.hitbtc.com/api/2/ws'
        self.connection = websocket.create_connection(self.url)

    def __connect__(self) -> None:

        self.connection.connect(self.url)

    def __disconnect__(self) -> None:

        self.connection.close()

    def __send__(self, method: str, rid: float = None, **params) -> int:

        if self.connection.connected:
            payload = {
                'method': method,
                'params': params,
                'id': rid or int(10000 * time.time())
            }
            payload = json.dumps(payload)
            return self.connection.send(payload)

    def __receive__(self) -> dict:

        try:
            response = json.loads(self.connection.recv())
            if isinstance(response, dict):
                error = response.get('error', None)
                if error:
                    error.update({
                        'error': True,
                        'id': response.get('id'),
                        'jsonrpc': response.get('jsonrpc'),
                    })
                    return error
            return response
        except Exception as error:
            return {
                'error': True,
                'message': error
            }
