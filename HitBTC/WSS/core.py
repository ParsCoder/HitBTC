from HitBTC.WSS.wss import Connection
import hashlib
import random
import hmac
import time


class HitBTC(object):

    def __init__(self, public: str, secret: str):

        self.params = dict()
        self.public = public
        self.secret = secret
        self.connection = Connection()
        self.connection.__connect__()
        self.authenticate()

    def __rand__(self):
        return int(random.randint(1, 9999999999999999) * time.time())

    def __clean__(self):
        self.params = {
            _key: _value for _key, _value in self.params.items() if _value is not None and _value
        }

    def authenticate(self, basic: bool = True, custom_nonce: str = None) -> None:

        if basic:
            algorithm = 'BASIC'
            secret = self.secret
            params = {'sKey': secret}
        else:
            algorithm = 'HS256'
            nonce = custom_nonce or str(round(time.time() * 91010109))
            raw_sig = (self.public + nonce).encode(encoding='UTF-8')
            signature = hmac.new(self.secret, raw_sig, hashlib.sha256).hexdigest()
            params = {'nonce': nonce, 'signature': signature}

        params.update(
            {
                'algo': algorithm,
                'pKey': self.public
            }
        )

        self.connection.__send__('login', **params)

    def get_currencies(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'currency': params.get('CURRENCY', None)
        }
        self.__clean__()
        currency = params.get('CURRENCY', None)
        if currency:
            method = 'getCurrency'
        else:
            method = 'getCurrencies'
        self.connection.__send__(method, rid, **self.params)
        return rid

    def get_symbols(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'symbol': params.get('SYMBOL', None)
        }
        self.__clean__()
        symbol = params.get('SYMBOL', None)
        if symbol:
            method = 'getSymbol'
        else:
            method = 'getSymbols'

        self.connection.__send__(method, rid, **self.params)
        return rid

    def get_trades(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'by': params.get('BY', None),
            'sort': params.get('SORT', None),
            'from': params.get('FROM', None),
            'till': params.get('TILL', None),
            'limit': params.get('LIMIT', None),
            'offset': params.get('OFFSET', None),
            'symbol': params.get('SYMBOL', None)
        }
        self.__clean__()
        self.connection.__send__('getTrades', rid, **self.params)
        return rid

    def get_trading_balance(self, **params) -> int:

        rid = self.__rand__()
        self.connection.__send__('getTradingBalance', rid, **params)
        return rid

    def get_orders(self, **params) -> int:

        rid = self.__rand__()
        self.connection.__send__('getOrders', rid, **params)
        return rid

    def subscribe_reports(self, status, **params) -> int:

        method = 'subscribeReports'
        rid = self.__rand__()
        if status is False:
            method = 'un' + method

        self.connection.__send__(method, rid, **params)
        return rid

    def subscribe_tickers(self, status, **params) -> int:

        method = 'subscribeTicker'
        rid = self.__rand__()
        if status is False:
            method = 'un' + method

        self.connection.__send__(method, rid, **params)
        return rid

    def subscribe_orderbook(self, status, **params) -> int:

        method = 'subscribeOrderbook'
        rid = self.__rand__()
        if status is False:
            method = 'un' + method

        self.connection.__send__(method, rid, **params)
        return rid

    def subscribe_trades(self, status, **params) -> int:

        method = 'subscribeTrades'
        rid = self.__rand__()
        if status is False:
            method = 'un' + method

        self.connection.__send__(method, rid, **params)
        return rid

    def subscribe_candles(self, status, **params) -> int:

        method = 'subscribeCandles'
        rid = self.__rand__()
        if status is False:
            method = 'un' + method

        self.connection.__send__(method, rid, **params)
        return rid

    def __place__(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'type': params.get('_TYPE', None),
            'price': params.get('PRICE', None),
            'symbol': params.get('SYMBOL', None),
            'clientOrderId': params.get('CID', None),
            'side': params.get('TYPE', None).lower(),
            'quantity': params.get('QUANTITY', None),
            'postOnly': params.get('POST_ONLY', None),
            'stopPrice': params.get('STOP_PRICE', None),
            'expireTime': params.get('EXPIRE_TIME', None),
            'timeInForce': params.get('TIME_IN_FORCE', None),
            'strictValidate': params.get('STRICT_VALIDATE', None)
        }
        self.__clean__()
        self.connection.__send__('newOrder', rid, **self.params)
        return rid

    def __cancel__(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'clientOrderId': params.get('CID', None)
        }
        self.__clean__()
        self.connection.__send__('cancelOrder', rid, **self.params)
        return rid

    def __replace__(self, **params) -> int:

        rid = self.__rand__()
        self.params = {
            'price': params.get('PRICE', None),
            'quantity': params.get('QUANTITY', None),
            'clientOrderId': params.get('CID', None),
            'requestClientId': params.get('_CID', None)
        }
        self.__clean__()
        self.connection.__send__('cancelReplaceOrder', rid, **self.params)
        return rid
