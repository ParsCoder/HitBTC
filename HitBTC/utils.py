class APIData(object):

    def __init__(self, response: list or dict):

        self.response = response

    def __currencies__(self) -> list or dict:
        if (isinstance(self.response, dict)
                and self.response.get('result', None) is not None):
            self.response = self.response.get('result')

        if isinstance(self.response, list):
            __response__ = []
            for _response_ in self.response:
                _response_ = {
                    'ID': StringConverter()(_response_.get('id')),
                    'CRYPTO': BooleanConverter()(_response_.get('crypto')),
                    'DELISTED': BooleanConverter()(_response_.get('delisted')),
                    'FULL_NAME': StringConverter()(_response_.get('fullName')),
                    'PAYOUT_FEE': FloatConverter()(_response_.get('payoutFee')),
                    'PAYIN_ENABLED': BooleanConverter()(_response_.get('payinEnabled')),
                    'PAYOUT_ENABLED': BooleanConverter()(_response_.get('payoutEnabled')),
                    'PAYIN_PAYMENT_ID': BooleanConverter()(_response_.get('payinPaymentId')),
                    'TRANSFER_ENABLED': BooleanConverter()(_response_.get('transferEnabled')),
                    'PAYIN_CONFIRMATIONS': IntegerConverter()(_response_.get('payinConfirmations')),
                    'PAYOUT_IS_PAYMENT_ID': BooleanConverter()(_response_.get('payoutIsPaymentId')),
                }
                __response__.insert(len(__response__), _response_)
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response

            __response__ = {
                'ID': StringConverter()(self.response.get('id')),
                'CRYPTO': BooleanConverter()(self.response.get('crypto')),
                'DELISTED': BooleanConverter()(self.response.get('delisted')),
                'FULL_NAME': StringConverter()(self.response.get('fullName')),
                'PAYOUT_FEE': FloatConverter()(self.response.get('payoutFee')),
                'PAYIN_ENABLED': BooleanConverter()(self.response.get('payinEnabled')),
                'PAYOUT_ENABLED': BooleanConverter()(self.response.get('payoutEnabled')),
                'PAYIN_PAYMENT_ID': BooleanConverter()(self.response.get('payinPaymentId')),
                'TRANSFER_ENABLED': BooleanConverter()(self.response.get('transferEnabled')),
                'PAYIN_CONFIRMATIONS': IntegerConverter()(self.response.get('payinConfirmations')),
                'PAYOUT_IS_PAYMENT_ID': BooleanConverter()(self.response.get('payoutIsPaymentId')),
            }
            return __response__

    def __symbols__(self) -> list or dict:
        if (isinstance(self.response, dict)
                and self.response.get('result', None) is not None):
            self.response = self.response.get('result')

        if isinstance(self.response, list):
            __response__ = []
            for _response_ in self.response:
                _response_ = {
                    'ID': StringConverter()(_response_.get('id')),
                    'TICK_SIZE': FloatConverter()(_response_.get('tickSize')),
                    'FEE_CURRENCY': StringConverter()(_response_.get('feeCurrency')),
                    'BASE_CURRENCY': StringConverter()(_response_.get('baseCurrency')),
                    'QUOTE_CURRENCY': StringConverter()(_response_.get('quoteCurrency')),
                    'QUANTITY_INCREMENT': FloatConverter()(_response_.get('quantityIncrement')),
                    'TAKE_LIQUIDITY_RATE': FloatConverter()(_response_.get('takeLiquidityRate')),
                    'PROVIDE_LIQUIDITY_RATE': FloatConverter()(_response_.get('provideLiquidityRate')),
                }
                __response__.insert(len(__response__), _response_)
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response

            __response__ = {
                'ID': StringConverter()(self.response.get('id')),
                'TICK_SIZE': FloatConverter()(self.response.get('tickSize')),
                'FEE_CURRENCY': StringConverter()(self.response.get('feeCurrency')),
                'BASE_CURRENCY': StringConverter()(self.response.get('baseCurrency')),
                'QUOTE_CURRENCY': StringConverter()(self.response.get('quoteCurrency')),
                'QUANTITY_INCREMENT': FloatConverter()(self.response.get('quantityIncrement')),
                'TAKE_LIQUIDITY_RATE': FloatConverter()(self.response.get('takeLiquidityRate')),
                'PROVIDE_LIQUIDITY_RATE': FloatConverter()(self.response.get('provideLiquidityRate')),
            }
            return __response__

    def __transactions__(self, snapshot: list or dict = None, limit: int = 100) -> list or dict:
        if isinstance(self.response, dict):
            if self.response.get('params', None) is not None:
                self.response = self.response.get('params')

            if self.response.get('data', None) is not None:
                self.response = self.response.get('data')

        if isinstance(self.response, list):
            __response__ = []
            if snapshot:
                __response__ = snapshot
            for _response_ in self.response:
                _response_ = {
                    'ID': StringConverter()(_response_.get('id')),
                    'FEE': FloatConverter()(_response_.get('fee')),
                    'HASH': StringConverter()(_response_.get('hash')),
                    'INDEX': IntegerConverter()(_response_.get('index')),
                    'CURRENCY': StringConverter()(_response_.get('currency')),
                    'TYPE': StringConverter()(_response_.get('type')).upper(),
                    'CREATED_AT': StringConverter()(_response_.get('createdAt')),
                    'UPDATED_AT': StringConverter()(_response_.get('updatedAt')),
                    'PAYMENT_ID': StringConverter()(_response_.get('paymentId')),
                    'STATUS': StringConverter()(_response_.get('status')).upper(),
                }

                if isinstance(__response__, list):
                    __response__.insert(len(__response__), _response_)
                elif isinstance(__response__, dict):
                    return _response_

                __response__.insert(len(__response__), _response_)
                __response__ = __response__[-limit:]
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response


    def __trades__(self, snapshot: list or dict = None, symbol: str = None, limit: int = 100) -> list or dict:
        if isinstance(self.response, dict):
            if self.response.get('params', None) is not None:
                self.response = self.response.get('params')

            __symbol__ = self.response.get('symbol', None)
            if not __symbol__ or symbol != __symbol__:
                return snapshot

            if self.response.get('data', None) is not None:
                self.response = self.response.get('data')

        if isinstance(self.response, list):
            __response__ = []
            if snapshot:
                __response__ = snapshot
            for _response_ in self.response:
                _response_ = {
                    'ID': IntegerConverter()(_response_.get('id')),
                    'FEE': FloatConverter()(_response_.get('fee')),
                    'PRICE': FloatConverter()(_response_.get('price')),
                    'OID': IntegerConverter()(_response_.get('orderId')),
                    'QUANTITY': FloatConverter()(_response_.get('quantity')),
                    'CID': StringConverter()(_response_.get('clientOrderId')),
                    'TYPE': StringConverter()(_response_.get('side')).upper(),
                    'DATETIME': StringConverter()(_response_.get('timestamp')),
                }
                if _response_.get('fee', None) is None:
                    _response_.pop('FEE')
                if _response_.get('orderId', None) is None:
                    _response_.pop('OID')
                if _response_.get('clientOrderId', None) is None:
                    _response_.pop('CID')

                if isinstance(__response__, list):
                    __response__.insert(len(__response__), _response_)
                elif isinstance(__response__, dict):
                    return _response_

                __response__.insert(len(__response__), _response_)
                __response__ = __response__[-limit:]
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response

    def __tickers__(self, snapshot: list or dict = None, symbol: str = None, limit: int = 100) -> list or dict:
        if isinstance(self.response, dict):
            __symbol__ = self.response.get('symbol', None)
            if not __symbol__ or symbol != __symbol__:
                return snapshot

            if self.response.get('params', None) is not None:
                self.response = self.response.get('params')

        if isinstance(self.response, list):
            __response__ = []
            if snapshot:
                __response__ = snapshot
            for _response_ in self.response:
                _response_ = {
                    'ASK': FloatConverter()(_response_.get('ask')),
                    'BID': FloatConverter()(_response_.get('bid')),
                    'LOW': FloatConverter()(_response_.get('low')),
                    'HIGH': FloatConverter()(_response_.get('high')),
                    'LAST': FloatConverter()(_response_.get('last')),
                    'OPEN': FloatConverter()(_response_.get('open')),
                    'VOLUME': FloatConverter()(_response_.get('volume')),
                    'SYMBOL': StringConverter()(_response_.get('symbol')),
                    'DATETIME': StringConverter()(_response_.get('timestamp')),
                    'VOLUME_QUOTE': FloatConverter()(_response_.get('volumeQuote')),
                }
                __response__.insert(len(__response__), _response_)
            __response__ = __response__[-limit:]
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response
            __response__ = {
                'ASK': FloatConverter()(self.response.get('ask')),
                'BID': FloatConverter()(self.response.get('bid')),
                'LOW': FloatConverter()(self.response.get('low')),
                'HIGH': FloatConverter()(self.response.get('high')),
                'LAST': FloatConverter()(self.response.get('last')),
                'OPEN': FloatConverter()(self.response.get('open')),
                'VOLUME': FloatConverter()(self.response.get('volume')),
                'SYMBOL': StringConverter()(self.response.get('symbol')),
                'DATETIME': StringConverter()(self.response.get('timestamp')),
                'VOLUME_QUOTE': FloatConverter()(self.response.get('volumeQuote')),
            }
            return __response__

    def __orderbook__(self, snapshot: list or dict = None, symbol: str = None, limit: int = 100) -> list or dict:
        if isinstance(self.response, dict):
            __symbol__ = self.response.get('symbol', None)
            if not __symbol__ or symbol != __symbol__:
                return snapshot

            if self.response.get('params', None) is not None:
                self.response = self.response.get('params')

        _error = self.response.get('ERROR')
        if _error:
            return self.response

        __response__ = {
            'ASK': [],
            'BID': [],
            'SEQUENCE': IntegerConverter()(self.response.get('sequence')),
            'DATETIME': StringConverter()(self.response.get('timestamp')),
        }

        if self.response.get('sequence', None) is None:
            __response__.pop('SEQUENCE')

        if snapshot is not None:
            __response__ = snapshot
            __response__['SEQUENCE'] = IntegerConverter()(self.response.get('sequence'))
            __response__['DATETIME'] = StringConverter()(self.response.get('timestamp'))

        _asks = self.response.get('ask')
        __asks__ = __response__.get('ASK')
        _bids = self.response.get('bid')
        __bids__ = __response__.get('BID')
        for ask in _asks:
            __asks__.insert(len(__asks__), {
                'SIZE': FloatConverter()(ask.get('size')),
                'PRICE': FloatConverter()(ask.get('price')),
            })
        __asks__ = __asks__[-limit:]
        __response__['ASK'] = __asks__
        for bid in _bids:
            __bids__.insert(len(__bids__), {
                'SIZE': FloatConverter()(bid.get('size')),
                'PRICE': FloatConverter()(bid.get('price')),
            })
        __bids__ = __bids__[-limit:]
        __response__['BID'] = __bids__
        return __response__

    def __candles__(self, snapshot: list or dict = None, symbol: str = None, limit: int = 100) -> list or dict:
        if isinstance(self.response, dict):
            __symbol__ = self.response.get('symbol', None)
            if not __symbol__ or symbol != __symbol__:
                return snapshot

            if self.response.get('params', None) is not None:
                self.response = self.response.get('params')

        if isinstance(self.response, list):
            __response__ = []
            if snapshot:
                __response__ = snapshot
            for _response_ in self.response:
                _response_ = {
                    'MIN': FloatConverter()(_response_.get('min')),
                    'MAX': FloatConverter()(_response_.get('max')),
                    'OPEN': FloatConverter()(_response_.get('open')),
                    'CLOSE': FloatConverter()(_response_.get('close')),
                    'VOLUME': FloatConverter()(_response_.get('volume')),
                    'DATETIME': StringConverter()(_response_.get('timestamp')),
                    'VOLUME_QUOTE': FloatConverter()(_response_.get('volumeQuote')),
                }
                if isinstance(__response__, list):
                    __response__.insert(len(__response__), _response_)
                elif isinstance(__response__, dict):
                    return _response_
            __response__ = __response__[-limit:]
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error:
                return self.response

    def __orders__(self):
        if isinstance(self.response, list):
            __response__ = []
            for _response_ in self.response:
                _response_ = {
                    'OID': IntegerConverter()(_response_.get('id')),
                    'PRICE': FloatConverter()(_response_.get('price')),
                    '_TYPE': StringConverter()(_response_.get('type')),
                    'SYMBOL': StringConverter()(_response_.get('symbol')),
                    'QUANTITY': FloatConverter()(_response_.get('quantity')),
                    'CID': StringConverter()(_response_.get('clientOrderId')),
                    'TYPE': StringConverter()(_response_.get('side').upper()),
                    'UPDATED_AT': StringConverter()(_response_.get('updatedAt')),
                    'CREATED_AT': StringConverter()(_response_.get('createdAt')),
                    'STATUS': StringConverter()(_response_.get('status').upper()),
                    'TIME_IN_FORCE': StringConverter()(_response_.get('timeInForce')),
                    '_CID': StringConverter()(_response_.get('originalRequestClientOrderId'))
                }
                if _response_.get('originalRequestClientOrderId', None) is None:
                    _response_.pop('_CID')
                __response__.insert(len(__response__), _response_)
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error is True:
                return self.response
            __response__ = {
                'OID': IntegerConverter()(self.response.get('id')),
                'PRICE': FloatConverter()(self.response.get('price')),
                '_TYPE': StringConverter()(self.response.get('type')),
                'SYMBOL': StringConverter()(self.response.get('symbol')),
                'QUANTITY': FloatConverter()(self.response.get('quantity')),
                'CID': StringConverter()(self.response.get('clientOrderId')),
                'TYPE': StringConverter()(self.response.get('side').upper()),
                'UPDATED_AT': StringConverter()(self.response.get('updatedAt')),
                'CREATED_AT': StringConverter()(self.response.get('createdAt')),
                'STATUS': StringConverter()(self.response.get('status').upper()),
                'TIME_IN_FORCE': StringConverter()(self.response.get('timeInForce')),
                '_CID': StringConverter()(self.response.get('originalRequestClientOrderId'))
            }
            if self.response.get('originalRequestClientOrderId', None) is None:
                __response__.pop('_CID')
            return __response__

    def __trading_balance__(self) -> list or dict:
        if isinstance(self.response, list):
            __response__ = []
            for _response_ in self.response:
                _response_ = {
                    'RESERVED': FloatConverter()(_response_.get('reserved')),
                    'CURRENCY': StringConverter()(_response_.get('currency')),
                    'AVAILABLE': FloatConverter()(_response_.get('available'))
                }
                __response__.insert(len(__response__), _response_)
            if len(__response__) == 1:
                return __response__[0]
            return __response__
        if isinstance(self.response, dict):
            _error = self.response.get('ERROR')
            if _error is True:
                return self.response
            __response__ = {
                'RESERVED': FloatConverter()(self.response.get('reserved')),
                'CURRENCY': StringConverter()(self.response.get('currency')),
                'AVAILABLE': FloatConverter()(self.response.get('available'))
            }


class StringConverter(object):

    def __call__(self, value):
        if value is not None:
            return str(value)


class BooleanConverter(object):

    def __call__(self, value):
        if value is not None:
            if isinstance(value, bool):
                return value
            value = str(value).lower()
            if value in ('yes', 'true', 't', '1'):
                return True
            if value in ('no', 'false', 'f', '0'):
                return False
        return None


class FloatConverter(object):

    def __call__(self, value):
        if value is not None:
            try:
                return float(value)
            except ValueError:
                return 0


class IntegerConverter(object):

    def __call__(self, value):
        if value is not None:
            try:
                return int(value)
            except ValueError:
                return 0


class CurrencyToSymbol(object):

    def __init__(self, market: object = None):
        self.market = market

    def __call__(self, currency: str = None):
        for _currency in ['USD', 'BTC', 'ETH']:
            data = self.market.get_symbols(SYMBOL=currency + _currency)
            error = data.get('ERROR')
            if error:
                data = self.market.get_symbols(SYMBOL=_currency + currency)
                error = data.get('ERROR')
                if error:
                    continue
            return data


class ToUSD(object):

    def __init__(self, core: object = None, market: object = None):
        self.core = core
        self.market = market

    def __call__(self, value: float or int, symbol: str = None, currency: str = None, price: bool = True):
        if currency:
            data = CurrencyToSymbol(market=self.market)(currency=currency)
            if not data:
                return int()
        else:
            data = self.market.get_symbols(required=True, SYMBOL=symbol)
        base = data.get('BASE_CURRENCY')
        quote = data.get('QUOTE_CURRENCY')
        if quote == 'USD':
            if price is True:
                base_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'USD', LIMIT=1)
                error = base_usd.get('ERROR') if isinstance(base_usd, dict) else None
                if base_usd and error is None:
                    return value * base_usd.get('OPEN', 1)
            return value
        elif quote == 'BTC':
            base_btc = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'BTC', LIMIT=1)
            error = base_btc.get('ERROR') if isinstance(base_btc, dict) else None
            if base_btc and error is None:
                btc_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL='BTC' + 'USD', LIMIT=1)
                if price is True:
                    return (value * base_btc.get('OPEN', 1)) * btc_usd.get('OPEN', 1)
                return value * btc_usd.get('OPEN', 1)
        elif quote == 'ETH':
            base_eth = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'ETH', LIMIT=1)
            error = base_eth.get('ERROR') if isinstance(base_eth, dict) else None
            if base_eth and error is None:
                eth_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL='ETH' + 'USD', LIMIT=1)
                if price is True:
                    return (value * base_eth.get('OPEN', 1)) * eth_usd.get('OPEN', 1)
                return value * eth_usd.get('OPEN', 1)
        return int()


class ToBTC(object):

    def __init__(self, core: object = None, market: object = None):
        self.core = core
        self.market = market

    def __call__(self, value: float or int, symbol: str = None, currency: str = None, price: bool = True):
        if currency:
            data = CurrencyToSymbol(market=self.market)(currency=currency)
            if not data:
                return int()
        else:
            data = self.market.get_symbols(required=True, SYMBOL=symbol)
        base = data.get('BASE_CURRENCY')
        quote = data.get('QUOTE_CURRENCY')
        if quote == 'USD':
            base_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'USD', LIMIT=1)
            error = base_usd.get('ERROR') if isinstance(base_usd, dict) else None
            if base_usd and error is None:
                btc_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL='BTC' + 'USD', LIMIT=1)
                if price is True:
                    return (value * base_usd.get('OPEN', 1)) / btc_usd.get('OPEN', 1)
                return value * btc_usd.get('OPEN', 1)
        elif quote == 'BTC':
            if price is True:
                base_btc = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'BTC', LIMIT=1)
                error = base_btc.get('ERROR') if isinstance(base_btc, dict) else None
                if base_btc and error is None:
                    return value * base_btc.get('OPEN', 1)
            return value
        elif quote == 'ETH':
            base_eth = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'ETH', LIMIT=1)
            error = base_eth.get('ERROR') if isinstance(base_eth, dict) else None
            if base_eth and error is None:
                eth_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL='ETH' + 'BTC', LIMIT=1)
                if price is True:
                    return (value * base_eth.get('OPEN', 1)) * eth_usd.get('OPEN', 1)
                return value * eth_usd.get('OPEN', 1)
        return int()


class ToETH(object):

    def __init__(self, core: object = None, market: object = None):
        self.core = core
        self.market = market

    def __call__(self, value: float or int, symbol: str = None, currency: str = None, price: bool = True):
        if currency:
            data = CurrencyToSymbol(market=self.market)(currency=currency)
            if not data:
                return int()
        else:
            data = self.market.get_symbols(required=True, SYMBOL=symbol)
        base = data.get('BASE_CURRENCY')
        quote = data.get('QUOTE_CURRENCY')
        if quote == 'USD':
            base_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'USD', LIMIT=1)
            error = base_usd.get('ERROR') if isinstance(base_usd, dict) else None
            if base_usd and error is None:
                btc_usd = self.market.get_candles(required=True, snapshot={}, SYMBOL='ETH' + 'USD', LIMIT=1)
                if price is True:
                    return (value * base_usd.get('OPEN', 1)) / btc_usd.get('OPEN', 1)
                return value * btc_usd.get('OPEN', 1)
        elif quote == 'BTC':
            base_btc = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'BTC', LIMIT=1)
            error = base_btc.get('ERROR') if isinstance(base_btc, dict) else None
            if base_btc and error is None:
                btc_eth = self.market.get_candles(required=True, snapshot={}, SYMBOL='ETH' + 'BTC', LIMIT=1)
                if price is True:
                    return (value * base_btc.get('OPEN', 1)) * btc_eth.get('OPEN', 1)
                return value * btc_eth.get('OPEN', 1)
        elif quote == 'ETH':
            if price is True:
                base_eth = self.market.get_candles(required=True, snapshot={}, SYMBOL=base + 'ETH', LIMIT=1)
                error = base_eth.get('ERROR') if isinstance(base_eth, dict) else None
                if base_eth and error is None:
                    return value * base_eth.get('OPEN', 1)
            return value
        return int()
