from HitBTC.utils import APIData


class Market(object):

    def __init__(self, core):
        self.core = core
        self.data = None
        self.required = None

    def __(self):
        if self.required is True and isinstance(self.data, dict):
            error = self.data.get('ERROR', None)
            if error:
                code = self.data.get('CODE', None)
                if code == 429:
                    return None
        return self.data

    def get_currencies(self, required: bool = False, **params) -> dict or list:
        while True:
            self.data = APIData(self.core.__send__('get', 'public/currency/{CURRENCY}'.format(
                CURRENCY=params.get('CURRENCY', '')
            ))).__currencies__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_symbols(self, required: bool = False, **params) -> dict or list:
        while True:
            self.data = APIData(self.core.__send__('get', 'public/symbol/{SYMBOL}'.format(
                SYMBOL=params.get('SYMBOL', '')
            ))).__symbols__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_trades(self, required: bool = False, snapshot: list or dict = None, **params) -> dict or list:
        _params = {
            'by': params.get('BY', ''),
            'sort': params.get('SORT', ''),
            'from': params.get('FROM', ''),
            'till': params.get('TILL', ''),
            'limit': params.get('LIMIT', 100),
            'offset': params.get('OFFSET', ''),
        }
        while True:
            self.data = APIData(self.core.__send__('get', 'public/trades/{SYMBOL}'.format(
                SYMBOL=params.get('SYMBOL', '')
            ), params={**_params})).__trades__(
                snapshot=snapshot, symbol=params.get('SYMBOL', ''), limit=params.get('LIMIT', 100)
            )
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_tickers(self, required: bool = False, **params) -> dict or list:
        while True:
            self.data = APIData(self.core.__send__('get', 'public/ticker/{SYMBOL}'.format(
                SYMBOL=params.get('SYMBOL', '')
            ))).__tickers__(symbol=params.get('SYMBOL', ''))
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_orderbook(self, required: bool = False, snapshot: list or dict = None, **params) -> dict or list:
        _params = {
            'limit': params.get('LIMIT', 100),
        }
        while True:
            self.data = APIData(self.core.__send__('get', 'public/orderbook/{SYMBOL}'.format(
                SYMBOL=params.get('SYMBOL', '')
            ), params={**_params})).__orderbook__(
                snapshot=snapshot, symbol=params.get('SYMBOL', ''), limit=params.get('LIMIT', 100)
            )
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_candles(self, required: bool = False, snapshot: list or dict = None, **params) -> dict or list:
        _params = {
            'sort': params.get('SORT', ''),
            'from': params.get('FROM', ''),
            'till': params.get('TILL', ''),
            'limit': params.get('LIMIT', 100),
            'offset': params.get('OFFSET', ''),
            'period': params.get('PERIOD', 'M1'),
        }
        while True:
            self.data = APIData(self.core.__send__('get', 'public/candles/{SYMBOL}'.format(
                SYMBOL=params.get('SYMBOL', '')
            ), params={**_params})).__candles__(
                snapshot=snapshot, symbol=params.get('SYMBOL', ''), limit=params.get('LIMIT', 100)
            )
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data
