import concurrent.futures
import requests
import json


class HitBTC(object):

    def __init__(self, public: str, secret: str, version: int = 2, speed: bool = False):

        self.url = 'https://api.hitbtc.com'
        self.speed = speed
        self.v2 = '/api/2'
        self.v1 = '/api/1'
        if version == 1:
            self.url += self.v1
        else:
            self.url += self.v2
        self.params = dict()
        self.public = public
        self.secret = secret
        self.request = None
        self.__auth__()

    def __auth__(self) -> None:
        self.request = requests.session()
        self.request.auth = (self.public, self.secret)

    def __send__(self, method: str, endpoint: str, params: dict = None) -> object:
        if self.request:
            connection = getattr(self.request, method.lower())

            def _request():
                response = connection('%s/%s' % (self.url, endpoint), params=params, data=params).json()
                if isinstance(response, dict):
                    error = response.get('error', None)
                    if error:
                        error = {
                            'ERROR': True,
                            'CODE': error.get('code'),
                            'MESSAGE': error.get('message'),
                            'DESCRIPTION': error.get('description')
                        }
                        return error
                return response

            def _thread():
                with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
                    future = {executor.submit(_request)}
                    for future in concurrent.futures.as_completed(future):
                        return future.result()

            try:
                if self.speed is True:
                    return _thread()
                return _request()
            except (requests.ReadTimeout, requests.ConnectionError):
                if self.speed is True:
                    return _thread()
                return _request()
            except json.decoder.JSONDecodeError:
                pass
