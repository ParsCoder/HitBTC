import datetime
from HitBTC.utils import APIData


class Trading(object):

    def __init__(self, core):
        self.core = core
        self.data = None
        self.report = None
        self.required = None

    def __(self, data: dict or list = None):
        if data is not None:
            self.data = data
        if self.required is True and isinstance(self.data, dict):
            error = self.data.get('ERROR', None)
            if error:
                code = self.data.get('CODE', None)
                if code == 429:
                    return None
        return self.data

    def get_orders(self, required: bool = False, **params) -> dict or list:
        _params = {
            'till': params.get('TILL', ''),
            'limit': params.get('LIMIT', 100),
            'offset': params.get('OFFSET', ''),
            'symbol': params.get('SYMBOL', ''),
            'clientOrderId': params.get('CID', ''),
            'from': params.get('FROM', (datetime.datetime.today() - datetime.timedelta(3 * 365 / 12)).isoformat() + 'Z')
        }
        while True:
            self.data = APIData(self.core.__send__('GET', 'history/order', params={**_params})).__orders__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data
    
    def get_transactions_history(self, required: bool = False, **params) -> dict or list:
        _params = {
            'id': params.get('ID', ''),
            'by': params.get('BY', ''),
            'sort': params.get('SORT', ''),
            'till': params.get('TILL', ''),
            'limit': params.get('LIMIT', 100),
            'offset': params.get('OFFSET', ''),
            'symbol': params.get('SYMBOL', ''),
            'currency': params.get('CURRENCY', ''),
            'from': params.get('FROM', (datetime.datetime.today() - datetime.timedelta(3 * 365 / 12)).isoformat() + 'Z')
        }
        while True:
            self.data = APIData(self.core.__send__('GET', 'account/transactions/{ID}'.format(
                ID=params.get('ID', '')
            ), params={**_params})).__transactions__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data
    
    def get_trades(self, required: bool = False, snapshot: list or dict = None, **params) -> dict or list:
        _params = {
            'by': params.get('BY', ''),
            'sort': params.get('SORT', ''),
            'till': params.get('TILL', ''),
            'limit': params.get('LIMIT', 100),
            'offset': params.get('OFFSET', ''),
            'symbol': params.get('SYMBOL', ''),
            'from': params.get('FROM', (datetime.datetime.today() - datetime.timedelta(3 * 365 / 12)).isoformat() + 'Z')
        }
        while True:
            self.data = APIData(self.core.__send__('GET', 'history/order', params={**_params})).__trades__(
                snapshot=snapshot
            )
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_active_orders(self, required: bool = False, **params) -> dict or list:
        _params = {
            'wait': params.get('WAIT', ''),
            'symbol': params.get('SYMBOL', '')
        }
        while True:
            self.data = APIData(self.core.__send__('GET', 'order/{CID}'.format(
                CID=params.get('CID', '')
            ), params={**_params})).__orders__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def get_trading_balance(self, required: bool = False, **params):
        while True:
            self.data = APIData(self.core.__send__('GET', 'trading/balance')).__trading_balance__()
            self.required = required
            self.data = self.__()
            if self.data:
                if isinstance(self.data, dict):
                    error = self.data.get('ERROR', None)
                else:
                    error = None
                if error is None:
                    for trading_balance in self.data:
                        if trading_balance.get('CURRENCY') == params.get('CURRENCY', ''):
                            return trading_balance
                return self.data

    def place(self, required: bool = False, **params):
        _params = {
            'type': params.get('_TYPE', ''),
            'price': params.get('PRICE', ''),
            'symbol': params.get('SYMBOL', ''),
            'quantity': params.get('QUANTITY', ''),
            'side': params.get('TYPE', '').lower(),
            'postOnly': params.get('POST_ONLY', ''),
            'stopPrice': params.get('STOP_PRICE', ''),
            'expireTime': params.get('EXPIRE_TIME', ''),
            'timeInForce': params.get('TIME_IN_FORCE', ''),
            'strictValidate': params.get('STRICT_VALIDATE', '')
        }
        while True:
            self.data = APIData(self.core.__send__('POST', 'order', params={**_params})).__orders__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data

    def replace(self, required: bool = False, **params):
        _params = {
            'PRICE': params.get('PRICE', ''),
            'QUANTITY': params.get('QUANTITY', '')
        }
        while True:
            i_response = self.get_active_orders(required=required, CID=params.get('CID', ''))
            if isinstance(i_response, dict):
                i_error = i_response.get('ERROR', None)
            else:
                i_error = None
            if i_error is True:
                return i_response
            _params.update({
                'TYPE': i_response.get('TYPE', ''),
                'SYMBOL': i_response.get('SYMBOL', '')
            })
            c_response = self.cancel(required=required, CID=params.get('CID', ''))
            if isinstance(c_response, dict):
                c_error = c_response.get('ERROR', None)
            else:
                c_error = None
            if c_error is True:
                return c_response
            response = self.place(required=required, **_params)
            return response

    def cancel(self, required: bool = False, **params):
        _params = {
            'symbol': params.get('SYMBOL', '')
        }
        while True:
            self.data = APIData(self.core.__send__('DELETE', 'order/{CID}'.format(
                CID=params.get('CID', '')
            ), params={**_params})).__orders__()
            self.required = required
            self.data = self.__()
            if self.data:
                return self.data
