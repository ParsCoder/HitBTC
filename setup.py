from setuptools import setup
from os import path


PATH = path.abspath(path.dirname(__file__))
with open(path.join(PATH, 'README.rst'), encoding='utf-8') as README:
    LONG_DESCRIPTION = README.read()

setup(
    name='hit-btc',
    version='0.28',
    license='MIT',
    description='A library for better communication with HitBTC Exchange API',
    long_description=LONG_DESCRIPTION,
    url='https://gitlab.com/ParsCoder/HitBTC',
    author='ParsCoder',
    author_email='parsian.coder@gmail.com',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    packages=['HitBTC', 'HitBTC.API', 'HitBTC.WSS'],
    keywords=['HitBTC', 'Websocket', 'API'],
    install_requires=['websocket-client', 'requests'],
    project_urls={
        'Source': 'https://gitlab.com/ParsCoder/HitBTC.git'
    },
)
